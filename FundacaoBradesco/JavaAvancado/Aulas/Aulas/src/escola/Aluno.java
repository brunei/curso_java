package escola;
public class Aluno {
    private String nome; // variável destinada ao nome do Aluno
    private int matricula; // variável destinada ao numero da matricula do aluno
    private String turno; // variável destinada ao turno que  o aluno estuda
    private int semestre = 1; // variável destinada ao semestre que o aluno está
    private int faltas = 0; // variável destinada a quantidade de faltas
      
    public Aluno(String nomeAluno, int numeroMatricula){
        this.nome = nomeAluno;
        this.matricula = numeroMatricula;
        
        System.out.println("Novo aluno - nome: "+this.nome+" - matricula: " + this.matricula);
    }
    
      //construtor
    public Aluno(){
        System.out.println("Construindo um aluno...");
    }
    
    public int proximoSemestre (){
        return this.semestre + 1;
    }
    
    public int quantidadeDeFaltas (){
        return this.faltas;
    }

    double calcularMediaProva(double d, double d0, double d1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

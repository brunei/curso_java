public class Caneta {
    
    String marca; // variavel destinada ao nome do fabricante
    boolean tinta; // variavel destinada a marcação se a caneta possui tinta ou não
    String corTinta; // variavel destinada a cor da tinta
    
    public Caneta(){
        marca = "Piolot";
        tinta = true;
        corTinta = "azul";
    }
    
}

public class NovoAluno {
    private int matricula; // variável destinada ao número da matricula do aluno
    
    //método setter atualiza a variável matricula
    public void setMatricula(int numeroMatricula){
        this.matricula = numeroMatricula;
    }
    // método getter que retorna variável matricula
    public int getMatricula(){
        return this.matricula;
    }
    
}

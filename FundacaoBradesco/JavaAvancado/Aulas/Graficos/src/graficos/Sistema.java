import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JProgressBar;
import javax.swing.JFileChooser;

public class Sistema {
    
    public static void main (String[] args){
        JFileChooser fileChooser = new JFileChooser();
        
        int retorno = fileChooser.showOpenDialog(null);
        
        if (retorno == JFileChooser.APPROVE_OPTION){
            File file = fileChooser.getSelectedFile();
            System.println(file.toString());
        }else{
            System.out.println("Nenhum arquivo foi selecionado");
        }
   
        JFrame frame = new JFrame ("Meu frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setLayout(null);
 
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Menu");
        JMenuItem menuItem = new JMenuItem("Olá!");
        JMenuItem menuItem2 = new JMenuItem("Oi");
        
        menu.add(menuItem);
        menu.add(menuItem2);
        menuBar.add(menu);
        
        JMenu menu2 = new JMenu("Cadastro");
        JMenuItem menu2Item = new JMenuItem("Funcionarios");
        JMenuItem menu2Item2 = new JMenuItem("Clientes");
               
        menu2.add(menu2Item);
        menu2.add(menu2Item2);
        menuBar.add(menu2);
        
        frame.setJMenuBar(menuBar);
                
        JTextField textField = new JTextField();
        
        textField.setBounds(20, 130, 360, 40);
        
        frame.add(textField);
        
        JCheckBox checkBox = new JCheckBox ("aceito");
        
        checkBox.setBounds(20, 180, 360, 40);
        
        frame.add(checkBox);
        
        JRadioButton radioButton = new JRadioButton("Sim");
        JRadioButton radioButton2 = new JRadioButton("Não");
        
        radioButton.setBounds(20, 210, 360, 40);
        radioButton2.setBounds(80, 210, 360, 40);
                
        frame.add(radioButton);
        frame.add(radioButton2);
    
        String[] itens = {"item1", "item2"};
        JList list = new JList(itens);
        
        list.setBounds(20, 70, 340, 40);
        
        frame.add(list);
        
        String[] box = {"Carro", "Moto", "Bicicleta"};
        JComboBox comboBox = new JComboBox(box);
        
        comboBox.setBounds(20, 25, 300, 30);
        
        frame.add(comboBox);
        
        JProgressBar progressBar = new JProgressBar(JProgressBar.HORIZONTAL, 1, 300);
        progressBar.setValue(70);
        
        progressBar.setBounds(20, 250, 300,15);
        
        frame.add(progressBar);
                
        frame.setVisible(true);
    }
    
    
}

package animais;
public class Animais {
    // variávle destinada ao tipo do animal (mamífero/répteis/aves...)
    private String tipo;
    
    //variável destinada ao nome 
    private String nome;
    
    private String animal;
    
    //gatter and setter tipo
    public String getTipo(){
        return this.tipo;
    }
    public void setTipo(String tipo){
        this.tipo = tipo;
    }
    
    //getter and setter nome
    public String getNome(){
        return this.nome;
    }
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public Animais(String animal){
        this.animal = animal;
    }
    
    public String getAnimal(){
        return this.animal;
    }
    
    //método que imprime no console o som do animal
    public void som(){
    }
    
}

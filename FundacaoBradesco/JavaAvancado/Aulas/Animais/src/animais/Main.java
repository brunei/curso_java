import animais.*;

public class Main {
    public static void main(String [] args){
        System.out.println("O som dos meus animais:");
        System.out.println();
        
        Gato meuGato = new Gato();
        meuGato.setNome("Mingau");
        System.out.println("O nome dele é " + meuGato.getNome());
        System.out.println("Meu " + meuGato.getAnimal() +" faz");
        meuGato.som();
        System.out.println("Tipo do animal: " + meuGato.getTipo());
        System.out.println();
                                      
        Gato animal1 = new Gato();
        animal1.setNome("Leão");
        System.out.println("O nome dele é " + animal1.getNome());
        System.out.println("Meu " + animal1.getAnimal() +" faz");
        animal1.som();
        System.out.println("Tipo do animal: " + animal1.getTipo());
        System.out.println();
                
        Sapo meuSapo = new Sapo();
        System.out.println("Meu " + meuSapo.getAnimal() +" faz");
        meuSapo.som();
        System.out.println("Tipo do animal: " + meuSapo.getTipo());    
        System.out.println();
        
        Cachorro meuCachorro = new Cachorro();
        System.out.println("Meu " + meuCachorro.getAnimal() +" faz");
        meuCachorro.som();
        System.out.println("Tipo do animal: " + meuCachorro.getTipo());
        System.out.println();
        
        Galinha minhaGalinha = new Galinha();
        System.out.println("Minha " + minhaGalinha.getAnimal() +" faz");
        minhaGalinha.som();
        System.out.println("Tipo do animal: " + minhaGalinha.getTipo());
        System.out.println();     
    }
    
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
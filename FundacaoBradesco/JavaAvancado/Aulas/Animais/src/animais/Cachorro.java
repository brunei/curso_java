package animais;
public class Cachorro extends Animais{
        
    public Cachorro (){
        super("Cachorro");
        this.setTipo("mamíferos");
    }
    
    //método que imprime o som do gato
    @Override
    public void som (){
        System.out.println("Au Au!");
    }
    
}

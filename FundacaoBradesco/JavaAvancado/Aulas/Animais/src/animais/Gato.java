package animais;
public class Gato extends Animais{
    
    public Gato (){
         super("Gato");
        this.setTipo("mamíferos");
    }
    
    //método que imprime o som do gato
    @Override
    public void som (){
        System.out.println("Miau!");
    }
    
}

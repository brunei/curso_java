package geometria;
public class Circulo extends Geometria{
    @Override
    public double area(){
        //Chamando um método da superclasse com o uso do operador ponto
        return 400;
    }
    
    @Override
    public double perimetro(){
        return 0;
    }
}

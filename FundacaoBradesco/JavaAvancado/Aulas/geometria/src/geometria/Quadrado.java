package geometria;
final class Quadrado extends Geometria{
    Quadrado(double lado){
        super();
    }
    
    @Override
    public double area(){
        //Chamando um metodo da superclasse com o uso do operadpr ponto
        return 200;
    }
    
    @Override
    public double perimetro(){
        return 0;
    }
}

public class WhileExemplo2 {
	public static void main(String [ ] args) {
		int num = 1;
		while (num <= 10) {
			System.out.println("4 x " + num + " = " + 4*num);
			num++;
		}
		// cada linha de comando da estrutura while em Java � limitada por ponto-e-virgula;
		// o agrupamento da estrutura while em Java e� limitado por chaves {......}
		//variavel num inicializada com 1 e incrementada de 1 num++ enquanto num<=10
		//Apresenta��o da mensagem concatenada "4x" com a variavel num e o sinal "="
		//execu��o da multiplica��o da tabuada do 4 com o valor acumulado em num
	}
}
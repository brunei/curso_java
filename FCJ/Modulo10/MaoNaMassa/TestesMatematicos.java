import java.util.Scanner;

public class TestesMatematicos{
	public static void main (String[] args){
		int quantidadeDeRespostasCorretas = 0;
		for(int i=0; i<10; i++){
			boolean numeroPar = i%2==0? true: false;
			if (numeroPar){
				boolean acertou = TestesMatematicos.testeDeMultiplicacao();
				if (acertou){
					quantidadeDeRespostasCorretas++;	
				}	
			}else{
				boolean acertou = TestesMatematicos.testeDeSoma();
				if (acertou){
					quantidadeDeRespostasCorretas++;
				}
			}
		}

		System.out.println("Você acertou " + quantidadeDeRespostasCorretas + " de 10 testes.");		
	}


	public static boolean testeDeMultiplicacao(){
		boolean resultado = false;
		Scanner scanner = new Scanner(System.in);
		int primeiroNumeroAleatorio = (int) (Math.random() * 11);//duvida se colocar assim vai de 0 a 10
		int segundoNumeroAleatotio = (int) (Math.random() * 10) + 1;// e assim vai de 1 a 10

		System.out.println("Quanto é " + primeiroNumeroAleatorio + " X " + segundoNumeroAleatotio + "? ");
		int resposta = scanner.nextInt();
		int resultadoDaMultiplicacao = primeiroNumeroAleatorio * segundoNumeroAleatotio;
		if (resposta == resultadoDaMultiplicacao){
			resultado = true;
			System.out.println("Parabens, você acertou! \n");
		}else{
			System.out.println("Você errou! A resposta correta é: " + resultadoDaMultiplicacao + "\n");
		}

		return resultado;
	}

	public static boolean testeDeSoma(){
		boolean resultado = false;
		Scanner scanner = new Scanner(System.in);
		int primeiroNumeroAleatorio = (int) (Math.random() * 10) + 1;
		int segundoNumeroAleatotio = (int) (Math.random() * 10) + 1;

		System.out.println("Quanto é " + primeiroNumeroAleatorio + " + " + segundoNumeroAleatotio + "? ");

		int resposta = scanner.nextInt();
		int resultadoDaSoma = primeiroNumeroAleatorio + segundoNumeroAleatotio;
		if(resposta == resultadoDaSoma){
			resultado = true;
			System.out.println("Parabens, você acertou!\n");
		}else{
			System.out.println("Você errou! A resposta certa é: " + resultadoDaSoma + "\n");
		}

		return resultado;
	}
}
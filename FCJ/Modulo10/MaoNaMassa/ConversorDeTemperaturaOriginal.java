import java.util.Scanner;
public class ConversorDeTemperatura{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);

		System.out.println("Por favor forneça a temperatura em graus celsius: ");
		double celsius = scanner.nextDouble();

		double convertidoParaFarenheit = ConversorDeTemperatura.celsiusParaFahrenheit(celsius);
		System.out.println("Temperatura em celsius: " + celsius + " ==> Equivalente em fahrenheit: " + convertidoParaFarenheit);


		System.out.println("Por favor forneça a temperatura em graus fahrenheit: ");
		double fahrenheit = scanner.nextDouble();

		double convertidoParaCelsius = ConversorDeTemperatura.fahrenheitParaCelsius(fahrenheit);
		System.out.println("Temperatura em fahrenheit: " + fahrenheit + " ==> Equivalente em fahrenheit: " + convertidoParaCelsius);

	}

	public static double converterCelsiusParaFahrenheit(double temperaturaEmCelsius){
		double fahrenheit = 9.0 / 5.0 * temperaturaEmCelsius + 32;
		return fahrenheit;

	}

	public static double converterFahrenheitParaCelsius(double temperaturaEmFarenheit){
		double celsius = 5.0 / 9.0 * (temperaturaEmFarenheit - 32);
		return celsius;

	}
}
import java.util.Scanner;

public class ConversorDeTemperatura{
	public static void main(String[] args){
		Scanner scanner = new Scanner (System.in);

		System.out.println("Por favor forneça a temperatura em graus Ceusius: ");
		double celsius = scanner.nextDouble();

		double convertidoParaFahrenheit = ConversorDeTemperatura.converterCelsiusParaFahrenheit(celsius);
		System.out.println("Temperatura em celsius: " + celsius + " ==> Equivalente em fahrenheit: " + convertidoParaFahrenheit);

		System.out.println("Por favor forneça a temperatura em graus fahreneit: ");
		double fahrenheit = scanner.nextDouble();

		double convertidoParaCelsius = ConversorDeTemperatura.converterFahrenheitParaCelsius(fahrenheit);
		System.out.println("Temperatura em fahrenheit: " + fahrenheit + " ==> Equivalente em celsius: " + convertidoParaCelsius);
	}

	public static double converterCelsiusParaFahrenheit(double temperaturaEmCelsius){
		double convertidoFahrenheit = 9.0 / 5.0 * temperaturaEmCelsius + 32;
		return convertidoFahrenheit;
	}	

	public static double converterFahrenheitParaCelsius(double temperaturaEmFahrenheit){
		double convertidoCelsius = 5.0 / 9.0 * (temperaturaEmFahrenheit - 32);
		return convertidoCelsius;
	}
}
import java.util.Properties;

public class SystemTest{
	public static void main(String[] args){
		String javaHome = System.getenv("JAVA_HOME");
		System.out.println("Variavel de sistema JAVA_HOME: " + javaHome);

		//obtenha as properties do seu sistema operacional
		Properties properties = System.getProperties();

		//imprime os propriedades do sistem no console
		properties.list(System.out);

		//obtém a hora atual do sistema operacional em milisegundos
		long currentTimeInMillis = System.currentTimeMillis();
		System.out.println("Hora atual em milisegundos: " + currentTimeInMillis);
	}
}
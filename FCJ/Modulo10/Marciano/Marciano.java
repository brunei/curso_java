public class Marciano{
	public static int contador = 0;
	private String nome;
	public Marciano(String nomeMarciano){
		contador++;
		nome = nomeMarciano;
	}

	public static int getTotalDeMarcianos(){
		return contador;
	}

	public void setNome(String nomeMarciano){
		nome = nomeMarciano;
	}

	public String getNome(){
		return nome;
	}
}
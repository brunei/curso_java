public class TesteSapato2{
	public static void main (String[] args){
		Sapato meuSapato = new Sapato();
		Sapato seuSapato = new Sapato();

		meuSapato.mostrarInformacoes();
		seuSapato.mostrarInformacoes();

		meuSapato.codigoCor = 'M';
		seuSapato.codigoCor = 'B';
		
		meuSapato.mostrarInformacoes();
		seuSapato.mostrarInformacoes();
	}
}
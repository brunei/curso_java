public class Sapato{
	public int codigo; //codigo default dos sapatos
	public String descricao;
	//codigos de cor validos: P=Preto; B=Branco; M=Marron
	public char codigoCor;
	public int tamanho;
	public double preco;
	//codigo genero validos: M=Mascolino; F=Feminino
	public char genero;
	public int quantidadeEmEstoque;
	//Este metodo exibe informações do sapato
	public void mostrarInformacoes(){
		System.out.println("Codigo: " + codigo);
		System.out.println("Descrição: " + descricao);
		System.out.println("Codigoda cor: " + codigoCor);
		System.out.println("Tamanho: " + tamanho);
		System.out.println("Preço: " + preco);
		System.out.println("Genero: " + genero);
		System.out.println("Quantidade em estoque: " + quantidadeEmEstoque);
	};//fim do metodo mostrarInformações
};//fim da classe.
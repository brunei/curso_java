public class RetanguloDoWhile{
	public static void main (String[] args){
	int altura = 15;
	int largura = 10;

	int contadorLinha = 0;
	int contadorColuna = 0;

	do {
		contadorColuna =0;
		do {
			System.out.print('#');
			contadorColuna++;
		}
		while (contadorColuna < largura); // fim do/while interno

		System.out.println();
		contadorLinha++;
		}
	while (contadorLinha < altura); // fim do/while externo
	}
}
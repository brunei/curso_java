public class RetanguloWhile{
	
	public static void main(String[] args){
		int altura = 6;
		int largura = 20;

		int contadorColuna = 0;
		int contadorLinha = 0;
		while (contadorLinha < altura){
			contadorColuna = 0;
			while(contadorColuna < largura){
				System.out.print("#");
				contadorColuna++;
			}
			System.out.println("");
			contadorLinha++;
		}
	}
}
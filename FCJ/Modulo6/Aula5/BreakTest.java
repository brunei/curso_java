//A instrução break sai de uma instrução for
public class BreakTest{
	public static void main (String[] args){
		int contador; //variavel de controle
		for(contador = 1; contador <=10; contador++){
			if(contador == 5) { // se o contador for igual a 5
				break; //termina o loop
			}
			System.out.printf("%d", contador);
		} //fim do for
		System.out.println("\nSaindo do loop com contador = " + contador);
	}//fim do método main
}//fim da classe BreakTest
public class FluxoDeComtroleComInstrucaoRotulada{
	public static void main(String[] args){
		rotulo1: System.out.println("Rotulo 1");
		rotulo2: System.out.println("Rotulo 2");

		rotulo3: {
			System.out.println("Rotulo 3");
			System.out.println("Rotulo 3 - Linha 3");
		}

		rotulo4:while(true){
			System.out.println("Rotulo 4");
			break rotulo4;
		}

		rotulo5:while(true){
					System.out.println("Inicio do corpo do rotulo 5");
				while(true){
					System.out.println("While dentro do rotulo 5");
					break rotulo5;
				}
		}
	}
}
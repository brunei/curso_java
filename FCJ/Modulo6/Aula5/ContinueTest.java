//instrução continue termina uma iteração de uma intrução for
public class ContinueTest{
	public static void main(String[] args){
		int contador;//variavel de controle
		for (contador = 1; contador <=10; contador++){ // faz o loop 10 vezes
			if(contador ==8){ // se contador for igual a 5
				continue; //pula o codigo restande do loop
			}
			System.out.printf("%d", contador);
		}//fim do contador
		System.out.println("\nUtilizando continue para pular a interacao 8.");
	}//fim do método main
}// fim da classe ContinueTest
public class ElevadorTeste{
	public static void main(String[] args){
		Elevador meuElevador = new Elevador();

		meuElevador.abrirPorta();
		meuElevador.fecharPorta();
		meuElevador.subir();
		meuElevador.abrirPorta();
		meuElevador.descer();
		meuElevador.subir();
		meuElevador.subir();
		meuElevador.abrirPorta();
		meuElevador.fecharPorta();
		meuElevador.descer();
		meuElevador.subir();
		meuElevador.abrirPorta();
		meuElevador.fecharPorta();
		meuElevador.subir();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
	}
}
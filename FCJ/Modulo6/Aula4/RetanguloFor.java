public class RetanguloFor{
	
	public int altura = 6;
	public int largura = 10;

	public void mostrarRetangulo(){

		for (int contadorLinha = 0; contadorLinha < altura; 
			contadorLinha++){
			for (int contadorColuna = 0; contadorColuna < largura; 
				contadorColuna++){
				System.out.print('#');
			}// fim do for interno
			System.out.println();
		}// fim do for
	}// fim do método mostrarRetangulo
}//fim da classe
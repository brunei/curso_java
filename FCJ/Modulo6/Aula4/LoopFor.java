public class LoopFor{
	public static void main (String[] args){
		Elevador elevador = new Elevador();
		for (int i = 0; i < elevador.ANDAR_MAXIMO; i++){
			elevador.subir();
		}
		System.out.println("Elevador no ultimo andar! Andar Corrrente: " + elevador.andarCorrente);
	}
}
public class SapatoConstrutorTeste{
	public static void main(String[] args){
		Sapato sapato1 = new Sapato();
		Sapato sapato2 = new Sapato('M');
		Sapato sapato3 = new Sapato('P');
		System.out.println("==== INFORMAÇÃO DO SAPATO 1 ====");
		sapato1.mostrarInformacoes();
		System.out.println("==== INFORMAÇÃO DO SAPATO 2====");
		sapato2.mostrarInformacoes();
		System.out.println("==== INFORMAÇÃO DO SAPATO 3 ====");
		sapato3.mostrarInformacoes();
	}
}
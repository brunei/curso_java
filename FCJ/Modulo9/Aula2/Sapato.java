public class Sapato{
	private long codigo = 0;
	private int quantidadeEmEstoque = 0;
	private double preco = 0;
	private String descricao = "Descrição requerida";
	private char cor = 'P';
	private int tamanho = 38;
	private char genero = 'M';

	//sobrescrevendo o construtor defout da classe
	public Sapato(){

	}

	//construindo o construtor personalizado
	public Sapato(char codigoCorInicial){
		switch (codigoCorInicial){
			case 'M':
			case 'P':
			case 'B':
			cor = codigoCorInicial;
			break;
			default:
				System.out.println("Código de cor Invalido! os códigos aceitos são M, P e B");
		}
	}

	/**
	* Este método exibe informações do sapato
	*/

	public void mostrarInformacoes(){
		System.out.println("Código: " + codigo);
		System.out.println("Descrição: " + descricao);
		System.out.println("Código de cor: " + cor);
		System.out.println("Tamanho: " + tamanho);
		System.out.println("Preço: " + preco);
		System.out.println("Gênero: " + genero);
		System.out.println("Quantidade em Estoque: " + quantidadeEmEstoque);
	}//fim do método mostrarInformacoes

	public void mostrarInformacoes(Sapato sapato){
		System.out.println("Código: " + sapato.codigo);
		System.out.println("Descrição: " + sapato.descricao);
		System.out.println("Código de cor: " + sapato.cor);
		System.out.println("Tamanho: " + sapato.tamanho);
		System.out.println("Preço: " + sapato.preco);
		System.out.println("Gênero: " + sapato.genero);
		System.out.println("Quantidade em Estoque: " + sapato.quantidadeEmEstoque);
	}
}//fim da classe
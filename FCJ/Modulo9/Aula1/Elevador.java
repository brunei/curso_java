public class Elevador{
	private boolean portaAberta = false; //portas estão fechadas por padrão
	private int andarCorrente = 1; //todos elevadores inicial no primeiro andar
	private final int CAPACIDADE = 1000;
	private final int ANDAR_MAXIMO =	10;
	private final int ANDAR_MINIMO = 1;

	public int getAndarCorrente(){
		return andarCorrente;
	}

	public void setAndarCorrente(int novoAndar){
		if(novoAndar > ANDAR_MAXIMO || novoAndar < ANDAR_MINIMO){
			System.out.println("Tentando acionar em andar: " + novoAndar);
		}else{
			System.out.println("Andar válido: " + novoAndar);
			andarCorrente = novoAndar;
		}
	}

	public boolean isPortaAberta(){
		return portaAberta;
	}

	public void setPortaAberta(boolean novoValorPortaAberta){
		portaAberta = novoValorPortaAberta;
	}
}
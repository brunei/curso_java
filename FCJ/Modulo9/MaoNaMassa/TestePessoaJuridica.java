import java.util.Scanner;

public class TestePessoaJuridica{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);

		System.out.println("Digite o seu id: ");
		Long id = scanner.nextLong();

		System.out.println("Disite a Razão Social: ");
		String razaoSocial = scanner.next();

		System.out.println("Digite o Nome Fantasia: ");
		String nomeFantasia = scanner.next();

		System.out.println("Digite o CNPJ: ");
		String cnpj = scanner.next();

		PessoaJuridica pessoaJuridica = new PessoaJuridica(id, razaoSocial, nomeFantasia, cnpj);

		System.out.println("Id: " + pessoaJuridica.getId());
		System.out.println("Razão Social: " + pessoaJuridica.getRazaoSocial());
		System.out.println("Nome Fantasia: " + pessoaJuridica.getNomeFantasia());
		System.out.println("CNPJ: " + pessoaJuridica.getCnpj());
	}
}
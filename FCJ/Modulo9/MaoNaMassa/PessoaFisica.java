public class PessoaFisica{
	private Long id;
	private String nome;
	private String sobrenome;
	private String cpf;

	public PessoaFisica(Long _id, String _nome, String _sobrenome, String _cpf){
		id = _id;
		nome = _nome;
		sobrenome = _sobrenome;
		cpf = _cpf;
	}

	public void setId(long _id){
		id = _id;
	}
	public Long getId(){
		return id;
	}

	public void setNome(String _nome){
		nome = _nome;
	}	
	public String getNome(){
		return nome;
	}

	public void setSobrenome(String _sobrenome){
		sobrenome = _sobrenome;
	}
	public String getSobrenome(){
		return sobrenome;
	}

	public void setCpf(String _cpf){
		cpf = _cpf;
	}
	public String getCpf(){
		return cpf;
	}
}
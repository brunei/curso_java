import java.util.Scanner;

public class TestePessoaFisica{
	public static void main(String[] args){
		
		Scanner scanner = new Scanner(System.in);

		System.out.println("Digite deu Id: ");
		Long id = scanner.nextLong();

		System.out.println("Digite seu Nome: ");
		String nome = scanner.next();

		System.out.println("Digite seu Sobrenome: ");
		String sobrenome = scanner.next();

		System.out.println("Digite seu CPF: ");
		String cpf = scanner.next();

		PessoaFisica pessoafisica = new PessoaFisica(id, nome, sobrenome, cpf);

		System.out.println("Id: " + pessoafisica.getId());
		System.out.println("Nome: " + pessoafisica.getNome());
		System.out.println("Sobrenome: " + pessoafisica.getSobrenome());
		System.out.println("CPF: " + pessoafisica.getCpf());
	}
}
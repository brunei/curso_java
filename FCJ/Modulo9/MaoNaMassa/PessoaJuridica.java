public class PessoaJuridica{
	private Long id;
	private String razaoSocial;
	private String nomeFantasia;
	private String cnpj;

	public PessoaJuridica(Long _id, String _razaoSocial, String _nomeFantasia, String _cnpj){
		id = _id;
		razaoSocial = _razaoSocial;
		nomeFantasia = _nomeFantasia;
		cnpj = _cnpj;
	}

	public void setId(Long _id){
		id = _id;
	}
	public Long getId(){
		return id;
	}

	public void setRazaoSocial(String _razaoSocial){
		razaoSocial = _razaoSocial;
	}
	public String getRazaoSocial(){
		return razaoSocial;
	}

	public void setNomeFantasia(String _nomeFantasia){
		nomeFantasia = _nomeFantasia;
	}
	public String getNomeFantasia(){
		return nomeFantasia;
	}

	public void setCnpj(String _cnpj){
		cnpj = _cnpj;
	}
	public String getCnpj(){
		return cnpj;
	}
}

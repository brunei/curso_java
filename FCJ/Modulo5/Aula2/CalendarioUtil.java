import java.util.Scanner;

public class CalendarioUtil{	
	public void calculoNumeroDeDias(){

		Scanner scanner = new Scanner(System.in);
		System.out.println("Digite o mes: ");
		int mes = scanner.nextInt();

		if (mes == 1 | mes == 3| mes == 5 | mes == 7 | mes == 8 | mes == 10 | mes == 12){
			System.out.println("Ha 31 dias neste mes.");
		} 
		else if (mes == 2){
			System.out.println("Ha 28 dias neste mes.");
		}
		else if (mes == 4 | mes == 6 | mes == 9 | mes == 11){
			System.out.println("Ha 30 dias neste mes.");
		}
		else {
			System.out.println("Mes invalido.");
		}
	}
}
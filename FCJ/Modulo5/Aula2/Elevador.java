public class Elevador{
	public boolean portaAberta = false; //portas estão fechadas por padrão
	public int andarCorrente = 1; //todos elevadores inicial no primeiro andar
	public final int ANDAR_MAXIMO =	10;
	public final int ANDAR_MINIMO = 1;

	public void abrirPorta(){
		System.out.println("Abrir porta.");
		portaAberta = true;
		System.out.println("Porta Aberta.");
	}
	public void fecharPorta(){
		System.out.println("Fechando porta.");
		portaAberta = false;
		System.out.println("Porta fechada.");
	}
	public void subir(){
		if(andarCorrente == ANDAR_MAXIMO){
			System.out.println("Impossivel subir.");
		}
		if (andarCorrente < ANDAR_MAXIMO){
			if (!portaAberta){
				System.out.println("Subindo 1 andar.");
				andarCorrente++;
				System.out.println("Andar: " + andarCorrente);
			}
		}
	}
	public void descer(){
		if(andarCorrente == ANDAR_MINIMO){
			System.out.println("Impossivel descer.");
		}else{
			if(!portaAberta){
				System.out.println("Descendo 1 andar.");
				andarCorrente--;
				System.out.println("Andar: " + andarCorrente);
			}
		}
	}
	public int getAndarCorrente(){
		return andarCorrente;
	}
	public boolean isPortaAberta(){
		return portaAberta;
	}
}
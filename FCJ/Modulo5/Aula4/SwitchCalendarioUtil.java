import java.util.Scanner;

public class SwitchCalendarioUtil {
	public void calculoDiasUteis(){

		Scanner scanner = new Scanner(System.in);
		System.out.println("Digite o mes: ");
		int mes = scanner.nextInt();

		switch(mes) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				System.out.println("Ha 31 dias neste mes.");
				break;
			case 2:
				System.out.println("Ha 28 dias neste mes.");
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				System.out.println("Ha 30 dias neste mes.");
				break;
			default:
				System.out.println("Mes invalido.");
		}
	}
}
import java.util.Scanner;

public class OperadorCondicionalTernario{
	public static void main (String[] args){
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Quantas vendas: ");
		int qnatidadeDeVendas = scanner.nextInt();

		int bonus = (qnatidadeDeVendas < 15)? 1000:2000;

		System.out.println("O valor do bonus: " + bonus);
	}
}
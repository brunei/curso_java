public class OperadorCondicionalCurtocircuito {
	
	public boolean trueMethod() {
		System.out.println("trueMethod");
		return true;
	}
	public boolean falseMethod(){
		System.out.println("falseMethod");
		return false;
	}
	public static void main(String[] args){
		OperadorCondicionalCurtocircuito o = new OperadorCondicionalCurtocircuito();
		System.out.println("Operador and '&&' de curto circuito");
		System.out.println("====================================");
		if(o.falseMethod() && o.trueMethod()){
			System.out.println("X");
		}

		System.out.println("\nOperador and '&' comum!");
		System.out.println("=========================");
		if(o.falseMethod() & o.trueMethod()){
			System.out.println("Y");
		}
	}
}
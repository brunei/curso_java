public class OperadorCondicional {
	
	public static void main (String[] args){
		double anoExperiencia = 2.8;
		int qntCertificacoes = 2;
		if (anoExperiencia <= 2 & qntCertificacoes == 0){
			System.out.println("Programador Junior");
		}
		if(anoExperiencia < 2 & qntCertificacoes >= 1){
			System.out.println("Programador Pleno");
		}
		if(anoExperiencia > 2 & qntCertificacoes >= 2){
			System.out.println("Programador Pleno 2");
		}
	}
}
public class OperadorCondicionalOuCurtocircuito {
	
	public static void main(String[] args){
		OperadorCondicionalCurtocircuito o = new OperadorCondicionalCurtocircuito();
		System.out.println("Operador and '||' de curto circuito");
		System.out.println("====================================");
		if(o.trueMethod() || o.falseMethod()){
			System.out.println("X");
		}

		System.out.println("\nOperador and '|' comum!");
		System.out.println("=========================");
		if(o.trueMethod() | o.falseMethod()){
			System.out.println("Y");
		}
	}
}
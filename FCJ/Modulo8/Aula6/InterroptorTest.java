public class InterroptorTest{
	public static void main(String[] args){
		Lampada lampada = new Lampada();
		System.out.println("Lampada acesa: " + lampada.acesa);

		Interroptor interroptor = new Interroptor();
		interroptor.acender(lampada);

		System.out.println("Lampada acesa: " + lampada.acesa);

		System.out.println("Potencia da lampada: " + lampada.potencia);

		interroptor.alterarPotencia(lampada.potencia);
		System.out.println("Potencia da lampada: " + lampada.potencia);
	}
}
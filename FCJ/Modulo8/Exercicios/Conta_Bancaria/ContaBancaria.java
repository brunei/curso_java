public class ContaBancaria{
	private String nomeDoCorrentista;
	private String nomeDoBanco;
	private Integer numeroDaAgencia;
	private Integer numeroDaContaCorrente;
	private Double saldoAtual;

	public ContaBancaria(){ //construtor defaul

	}
	//criando o construtor Conta Corrente
	public ContaBancaria(String novoNomeDoCorrentista, String novoNomeDoBanco, Integer novoNumeroDaAgencia, Integer novoNumeroDaContaCorrente, Double novoSaldoAtual){
		nomeDoCorrentista = novoNomeDoCorrentista;
		nomeDoBanco = novoNomeDoBanco;
		numeroDaAgencia = novoNumeroDaAgencia;
		numeroDaContaCorrente = novoNumeroDaContaCorrente;
		saldoAtual = novoSaldoAtual;
	}


	public void setNomeDoCoerrentista(String _nome){
		nomeDoCorrentista = _nome;
	}
	public void setNomeDoBanco(String _nomeDoBanco){
		nomeDoBanco = _nomeDoBanco;
	}
	public void setNumeroDaAgencia(Integer _numeroDaAgencia){
		numeroDaAgencia = _numeroDaAgencia;
	}
	public void setNumeroDaContaCorrente(Integer _numeroDaContaCorrente){
		numeroDaContaCorrente = _numeroDaContaCorrente;
	}
	public void setSaldoAtual(Double _saldoAtual){
		saldoAtual = _saldoAtual;
	}

	public String getNomeDoCorrentista(){
		return nomeDoCorrentista;
	}
	public String getNomeDoBanco(){
		return nomeDoBanco;
	}
	public Integer getNumeroDaAgencia(){
		return numeroDaAgencia;
	}
	public Integer getNumeroDaContaCorrente(){
		return numeroDaContaCorrente;
	}
	public Double getSaldoAtual(){
		return saldoAtual;
	}
	// criando objeto conta bancaria
	public ContaBancaria criarContaBancaria(String novoNomeDoCorrentista, String novoNomeDoBanco, Integer novoNumeroDaAgencia, Integer novoNumeroDaContaCorrente, Double novoSaldoAtual){
		ContaBancaria conta = new ContaBancaria();
		conta.setNomeDoCoerrentista(novoNomeDoCorrentista);
		conta.setNomeDoBanco(novoNomeDoBanco);
		conta.setNumeroDaAgencia(novoNumeroDaAgencia);
		conta.setNumeroDaContaCorrente(novoNumeroDaContaCorrente);
		conta.setSaldoAtual(novoSaldoAtual);
		return conta;
	}

	public void depositar(Double valorDoDeposito){
		saldoAtual = saldoAtual + valorDoDeposito;
		System.out.println("Novo Saldo: R$" + saldoAtual);
	}

	public void efetuarSaque(Double valorDoSaque){
		if(valorDoSaque > saldoAtual){
			System.out.println("Você não tem saldo suficiente para saque " + valorDoSaque + ". Voce tem: " + saldoAtual);
		}else{
			saldoAtual = saldoAtual - valorDoSaque;
			System.out.println("Saque de R$ "+ valorDoSaque + " Efetuado com sucesso.");
			System.out.println("Salto atual é: R$" + saldoAtual);
		}
	}
}
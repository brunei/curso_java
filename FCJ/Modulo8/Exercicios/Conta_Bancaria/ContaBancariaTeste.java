import java.util.Scanner;
public class ContaBancariaTeste{
	public static void main (String[] args){
		// criando o objeto com construtor personalizado
		//ContaBancaria contaBancaria = new ContaBancaria("Correntista ", "Nome do Banco ", 20, 9090, 100.00);
		
		// criando sem o construtor personalizado
		ContaBancaria contaBancaria = new ContaBancaria();
		contaBancaria = contaBancaria.criarContaBancaria("Correntista ", "Nome do Banco ", 20, 9090, 100.00);
		
		Scanner scanner = new Scanner(System.in);

		System.out.println("Digite o valor do depósito: ");
		Double deposito = scanner.nextDouble();
		contaBancaria.depositar(deposito);

		System.out.println("Digite o valor do saque: ");
		Double saque = scanner.nextDouble();
		contaBancaria.efetuarSaque(saque);
	}
}
import java.util.Scanner;
public class ImcTest{
	
	public static void main(String[] args){
		
		Imc imc = new Imc();

		Scanner scanner = new Scanner(System.in);
		

		System.out.println("A Peso em kg: ");
		int peso = scanner.nextInt();
		
		System.out.println("O Altura em metros: ");
		double altura = scanner.nextDouble();

		imc.setPeso(peso);
		imc.setAltura(altura);

		double resultado = imc.calculoImc();
		System.out.println(resultado);

		if(resultado < 18.5){
			System.out.println("Seu peso é: " + resultado + " Abaixo do peso.");
		}else if(resultado > 18.5 & resultado < 24.9){
			System.out.println("Seu peso é: " + resultado + " Peso normal.");
		}else if(resultado > 25 & resultado < 29.9){
			System.out.println("Seu peso é: " + resultado + " Sobrepeso.");
		}else if(resultado > 30 & resultado < 34.9){
			System.out.println("Seu peso é: " + resultado + " Obesidade Grau 1.");
		}else if(resultado > 35 & resultado < 39.9){
			System.out.println("Seu peso é: " + resultado + " Obesidade Grau 2.");
		}else{ 
			System.out.println("Seu peso é: " + resultado + " Obesidade Grau 3 ou Mórbida.");
		}
	}
}
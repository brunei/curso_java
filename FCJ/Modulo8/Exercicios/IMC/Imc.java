public class Imc{
	
	private Integer peso;
	private Double altura;
	
	public Imc(){//construtor defaut

	}
	//construtor 
	public Imc(Integer valPeso, Double valAltura){
		peso = valPeso;
		altura = valAltura;
	}

	public void setPeso(Integer _peso){
		peso = _peso;
	} 
	public void setAltura(Double _altura){
		altura = _altura;
	}
	public Integer getPeso(){
		return peso;
	}
	public Double getAltura(){
		return altura;
	}

	public double calculoImc(){
		double formula = (peso/(altura*altura));
		return formula;		
	}
}
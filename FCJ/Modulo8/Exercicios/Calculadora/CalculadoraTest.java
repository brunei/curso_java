import java.util.Scanner;

public class CalculadoraTest{
	
	public static void main(String [] args){
		Scanner scanner = new Scanner(System.in);
		Calculadora calculadora = new Calculadora();

		
		System.out.println("Digite o primeiro numero: ");
		int numA = scanner.nextInt();
		System.out.println("Qual é a operação?");
		String operacao = scanner.next();
		System.out.println("Digite o segundo numero: ");
		int numB = scanner.nextInt();

		if(operacao.equals("+")){
			int soma = calculadora.somar(numA, numB);
			System.out.println(soma);	
		}else if(operacao.equals("-")){
			int subtracao = calculadora.subtrair(numA, numB);
			System.out.println(subtracao);
		}else if(operacao.equals("*")){
			int mutiplica = calculadora.mutiplicar(numA, numB);
			System.out.println(mutiplica);
		}else if(operacao.equals("/")){
			int divisao = calculadora.dividir(numA, numB);
			System.out.println(divisao);
		}else{
			System.out.println("Operação invalida.");
		}
	}
}
public class Calculadora{
	
	public int somar(int num1,int num2){
		System.out.println("Resultado da Soma: ");
		return num1 + num2;
	}
	public long somar(long num1,long num2){
		System.out.println("Resultado da Soma: ");
		return num1 + num2;
	}

	public double somar(double num1,double num2){
		System.out.println("Resultado da Soma: ");
		return num1 + num2;
	}
	public float somar(float num1,float num2){
		System.out.println("Resultado da Soma: ");
		return num1 + num2;
	}

	public int subtrair(int num1, int num2){
		System.out.println("Resultado da Subtração: ");
		return num1 - num2;
	}
	public long subtrair(long num1, long num2){
		System.out.println("Resultado da Subtração: ");
		return num1 - num2;
	}
	public double subtrair(double num1, double num2){
		System.out.println("Resultado da Subtração: ");
		return num1 - num2;
	}
	public float subtrair(float num1, float num2){
		System.out.println("Resultado da Subtração: ");
		return num1 - num2;
	}

	public int mutiplicar(int num1, int num2){
		System.out.println("Resultado da Mutiplicação: ");
		return num1 * num2;
	}
	public long mutiplicar(long num1, long num2){
		System.out.println("Resultado da Mutiplicação: ");
		return num1 * num2;
	}
	public double mutiplicar(double num1, double num2){
		System.out.println("Resultado da Mutiplicação: ");
		return num1 * num2;
	}
	public float mutiplicar(float num1, float num2){
		System.out.println("Resultado da Mutiplicação: ");
		return num1 * num2;
	}

	public int dividir(int num1, int num2){
		System.out.println("Resultado da Divisão: ");
		return num1 / num2;
	}
	public long dividir(long num1, long num2){
		System.out.println("Resultado da Divisão: ");
		return num1 / num2;
	}
	public double dividir(double num1, double num2){
		System.out.println("Resultado da Divisão: ");
		return num1 / num2;
	}
	public float dividir(float num1, float num2){
		System.out.println("Resultado da Divisão: ");
		return num1 / num2;
	}
}
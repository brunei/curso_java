public class CalculadoraTest{
	
	public static void main (String[] args){

		Calculadora calculadora = new Calculadora();

		int total1 = calculadora.somar(3,4);
		System.out.println(total1);

		double total2 = calculadora.somar(1.2,3.8);
		System.out.println(total2);

		long total3 = calculadora.somar(4l, 7l);
		System.out.println(total3);

		float total4 = calculadora.somar(3, 4.8f);
		System.out.println(total4);
	}
}
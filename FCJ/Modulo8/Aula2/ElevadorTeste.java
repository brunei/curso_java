public class ElevadorTeste{
	public static void main(String[] args){
		Elevador meuElevador = new Elevador();

		meuElevador.abrirPorta();
		meuElevador.fecharPorta();
		meuElevador.subir();
		meuElevador.abrirPorta();
		meuElevador.fecharPorta();
		meuElevador.descer();
		meuElevador.subir();
		meuElevador.subir();
		meuElevador.setAndar(meuElevador.PRIMEIRO_ANDAR);
		meuElevador.abrirPorta();
		meuElevador.fecharPorta();
		meuElevador.descer();
		meuElevador.subir();
		meuElevador.abrirPorta();
		meuElevador.fecharPorta();
		meuElevador.subir();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.descer();
		meuElevador.setAndar(meuElevador.ULTIMO_ANDAR);

		int andarCorrente = meuElevador.getAndar();
		System.out.println("Andar Corrente: " + andarCorrente);

		meuElevador.setAndar(5);
	}
}
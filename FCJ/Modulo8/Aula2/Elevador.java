public class Elevador{
	public boolean portaAberta = false; //portas estão fechadas por padrão
	public int andarCorrente = 1; //todos elevadores inicial no primeiro andar
	public final int CAPACIDADE = 800;
	public final int ULTIMO_ANDAR =	10;
	public final int PRIMEIRO_ANDAR = 1;

	public void abrirPorta(){
		System.out.println("Abrir porta.");
		portaAberta = true;
		System.out.println("Porta Aberta.");
	}
	public void fecharPorta(){
		System.out.println("Fechando porta.");
		portaAberta = false;
		System.out.println("Porta fechada.");
	}
	public void subir(){
		if(andarCorrente == ULTIMO_ANDAR){
			System.out.println("Impossivel subir.");
		}
		if (andarCorrente < ULTIMO_ANDAR){
			if (!portaAberta){
				System.out.println("Subindo 1 andar.");
				andarCorrente++;
				System.out.println("Andar: " + andarCorrente);
			}
		}
	}
	public void descer(){
		if(andarCorrente == PRIMEIRO_ANDAR){
			System.out.println("Impossivel descer.");
		}else{
			if(!portaAberta){
				System.out.println("Descendo 1 andar.");
				andarCorrente--;
				System.out.println("Andar: " + andarCorrente);
			}
		}
	}
	public void setAndar(int andarDesejado){
		while (andarCorrente != andarDesejado){
			if (andarCorrente < andarDesejado){
				subir();
			}else{
				descer();
			}
		}
	}
	public int getAndar(){
		return andarCorrente;
	}
	public boolean isPortaAberta(){
		return portaAberta;
	}
}
public class TesteIncremento{
	public static void main(String[] args) {
		int contador = 15;
		int a, b, c, d;
		a = contador++;
		b = contador;
		c = ++contador;
		d = contador;
		System.out.println(a + ", " + b + ", " + c + ", " + d + ".");
	}
}
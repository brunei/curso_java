public class Pessoa{
	public int idadeEmAnos = 35;

	public void calculaIdade(){

		int idadeEmDias = idadeEmAnos * 365;
		long idadeEmSegundos = idadeEmAnos * 365 * 24L * 60 * 60;

		System.out.println("Voce tem " + idadeEmDias + "dias vividos.");
		System.out.println("Voce tem " + idadeEmSegundos + " de tempo vividos.");
	}	
}
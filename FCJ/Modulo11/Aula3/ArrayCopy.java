public class ArrayCopy{
	
	public static void imprimirConteudoDoArrayNoConsole(int arrayImprimir[]){
		int posicaoUltimoIntemDoArrayMaior = arrayImprimir.length -1;
		for (int i= 0; i< arrayImprimir.length; i++){

			if(i == posicaoUltimoIntemDoArrayMaior){
				System.out.println(arrayImprimir[i]);
			}else{
				System.out.print(arrayImprimir[i] + ",");
			}
		}
	}

	public static void main(String[] args){
		//array origem
		int arrayMenor[] = {1,2,3,4};
		System.out.print("###### ARRAY MENOR ######");
		imprimirConteudoDoArrayNoConsole(arrayMenor);
		System.out.println("");
		
		//array destino
		int arrayMaior[] = {10,9,8,7,6,5,4,3,2,1};
		System.out.println("###### Array Maior antes de copiar do menor ######");
		imprimirConteudoDoArrayNoConsole(arrayMaior);
		System.out.println("");
		
		//imprime o conteudo da array maior no console após copiar o conteudo do menor para o array maior
		System.arraycopy(arrayMenor, 0, arrayMaior, 0, arrayMenor.length);
		System.out.println("#### Array Maior após a copia do menor: #####");
		imprimirConteudoDoArrayNoConsole(arrayMaior);
	}
}
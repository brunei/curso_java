public class Produto{
	private String nome;
	private double preco;

	public void setNome(String novoNome){
		nome = novoNome;
	}
	public void setPreco(double novoPreco){
		preco = novoPreco;
	}

	
	public String getNome(){
		return nome;
	}
	public double getPreco(){
		return preco;
	}
}
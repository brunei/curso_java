public class PessoaFisica{
	private static long contadorDePessoaCadastrada = 0;
	private long id;
	private String nome;
	private String sobrenome;
	private String telefoneComDDD;

	public void setId(long novoId){
		id = novoId;
	}
	public void setNome(String novoNome){
		nome = novoNome;
	}
	public void setSobrenome(String novoSobrenome){
		sobrenome = novoSobrenome;
	}
	public void setTelefoneComDDD(String novoTelefoneComDDD){
		telefoneComDDD = novoTelefoneComDDD;
	}

	public long getId(){
		return id;
	}
	public String getNome(){
		return nome;
	}
	public String getSobrenome(){
		return sobrenome;
	}
	public String getTelefoneComDDD(){
		return telefoneComDDD;
	}

	public PessoaFisica(String novoNome, String novoSobrenome, String novoTelefoneComDDD){
		setId(getId() + 1);
		setNome(novoNome); 
		setSobrenome(novoSobrenome);
		setTelefoneComDDD(novoTelefoneComDDD);
	}

	public PessoaFisica(){
		setId(0);
		setNome(""); 
		setSobrenome("");
		setTelefoneComDDD("");
	}

}
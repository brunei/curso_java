public class Cardapio{
	private Produto[] listaDeProdutos;
	public Cardapio(int tamanho){
		listaDeProdutos = new Produto[tamanho];
	}

	public void adicionarProduto(Produto produto){
		for(int i=0; i<listaDeProdutos.length; i++){
			if(listaDeProdutos[i] == null){
				//coloca o produto na proxima posição vazia do array de produtos
				listaDeProdutos[i] = produto;
				produto.setId(i);
				break;
			}
		}
	}

	public Produto getProduto(int idPosicaoDoProduto){
		return listaDeProdutos[idPosicaoDoProduto];
	}

	public void listaDeProdutos(){
		System.out.println("#### LISTANDO PRODUTOS DO CARDAPIO ####\n\n");
		for(Produto produto: listaDeProdutos){
			if(produto !=null){
				System.out.println("Id: " + produto.getId() + "; Produto: " + produto.getNome() + "; Preço R$" + produto.getPreco());
			}
		}
	}
}
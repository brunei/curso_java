public class Produto{
	private int id;
	private String nome;
	private double preco;

	public void setId(int novoId){
		id = novoId;
	}
	public void setNome(String novoNome){
		nome = novoNome;
	}
	public void setPreco(double novoPreco){
		preco = novoPreco;
	}

	public int getId(){
		return id;
	}
	public String getNome(){
		return nome;
	}
	public double getPreco(){
		return preco;
	}
}
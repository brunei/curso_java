import java.util.Scanner;
public class SistemaDeRestaurante{
	private static Cardapio cardapio = new Cardapio (10);
	public static void main (String[] args){
		while(true){
			exibirOpcoesDoSistema();
		}
	}

	public static void exibirCardapio(){
		cardapio.listaDeProdutos();
		exibirOpcoesDoSistema();
	}

	public static void buscarPrecoDeProduto(){
		System.out.println("Didgite o código do produto: ");
		Scanner scanner = new Scanner(System.in);
		int codigoDoProduto = scanner.nextInt();
		Produto produto = cardapio.getProduto(codigoDoProduto);
		if(produto !=null){
			System.out.println("PRODUTO ENCONTRADO");
			System.out.println("Produto: " + produto.getNome() + "; preço: R$" + produto.getPreco());
			exibirOpcoesDoSistema();
		}else{
			System.out.println("Nenhum produto encontrado!");
			exibirOpcoesDoSistema();
		}
	}

	public static void cadastrarNovoProduto(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("Digite o nome do produto: ");
		String nome = scanner.nextLine();
		System.out.println("Digite o preço do produto: ");
		Double valor = scanner.nextDouble();
		Produto produto = new Produto();
		produto.setNome(nome);
		produto.setPreco(valor);
		cardapio.adicionarProduto(produto);
		System.out.println("Produto cadastrado com sucesso!");
		System.out.println();
		exibirOpcoesDoSistema();
	}

	public static void exibirOpcoesDoSistema(){
		System.out.println("###########################################");
		System.out.println("");
		System.out.println(" Selecione uma opções abaixo: ");
		System.out.println("");
		System.out.println("1 - Para cadastrar novo produto.");
		System.out.println("2 - Para exibir cardápio.");
		System.out.println("3 - Para buscar preço de produto.");
		System.out.println("4 - Para sair do sistema.");
		Scanner scanner = new Scanner(System.in);
		int opcao = scanner.nextInt();
		if(opcao == 1){
			cadastrarNovoProduto();
		}else if (opcao == 2){
			exibirCardapio();
		}else if (opcao == 3){
			buscarPrecoDeProduto();
		}else if (opcao == 4){
			System.out.println("Saindo do sistema... ");
			System.exit(0);
		}else{
			System.out.println("OPÇÃO INVALIDA! Por favor selecione uma opção valida entre 1 e 4.");
			System.out.println(" Selecione uma opções abaixo: ");
			System.out.println("1 - Para cadastrar novo produto.");
			System.out.println("2 - Para exibir cardápio.");
			System.out.println("3 - Para buscar preço de produto.");
			System.out.println("4 - Para sair do sistema.");
		}
	}
}
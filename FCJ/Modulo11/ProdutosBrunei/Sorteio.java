import java.util.Scanner;
import java.util.Random;

public class Sorteio{
	private static Cadastro cadastro = new Cadastro (3);
	
	public static void main (String[] args){	
		while(true){
			exibirOpcoesDoSistema();
		}
	}

	public static void exibirCastro(){
		cadastro.listaDePessoas();
		exibirOpcoesDoSistema();
	}

	public static void cadastrarNovaPessoa(){
		for(int i = 0; i < 3; i++){
		System.out.println("Cadastrando nova Pessoa " + (i + 1));
			Scanner scanner = new Scanner(System.in);
			System.out.println("Digite o Seu Nome: ");
			String nome = scanner.nextLine();
			System.out.println("Digite o Seu Sobrenome: ");
			String sobrenome = scanner.nextLine();
			System.out.println("Digite o seu numero de Telefone com DDD: ");
			String telefone = scanner.nextLine();
			PessoaFisica pessoa = new PessoaFisica();
			pessoa.setNome(nome);
			pessoa.setSobrenome(sobrenome);
			pessoa.setTelefoneComDDD(telefone);
			cadastro.adicionarPessoa(pessoa);
			System.out.println("Pessoa cadastrada com sucesso!\n");
		}
	}

	public static void gerarSorteio(){
		Random random = new Random();
		int sorteioDosCadastrados = random.nextInt(3);
		System.out.println("A pessoa sorteada foi a com o ID nuemro: " + sorteioDosCadastrados);
	}

	public static void exibirOpcoesDoSistema(){
		System.out.println("###########################################");
		System.out.println("");
		System.out.println(" Selecione uma opções abaixo: ");
		System.out.println("");
		System.out.println("1 - Para cadastrar nova pessoa.");
		System.out.println("2 - Para exibir todos cadastrados para o sorteio.");
		System.out.println("3 - Gerar Sorteio.");
		System.out.println("4 - Para sair do sistema.");
		Scanner scanner = new Scanner(System.in);
		int opcao = scanner.nextInt();
		if(opcao == 1){
			cadastrarNovaPessoa();
		}else if (opcao == 2){
			exibirCastro();
		}else if (opcao == 3){
			gerarSorteio();
		}else if (opcao == 4){
			System.out.println("Saindo do sistema... ");
			System.exit(0);
		}else{
			System.out.println("OPÇÃO INVALIDA! Por favor selecione uma opção valida entre 1 e 4.");
			System.out.println(" Selecione uma opções abaixo: ");
			System.out.println("1 - Para cadastrar nova pessoa.");
			System.out.println("2 - Para exibir todos cadastrados para o sorteio.");
			System.out.println("3 - Gerar Sorteio.");
			System.out.println("4 - Para sair do sistema.");
		}
	}
}
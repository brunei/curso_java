public class Cadastro{
	private PessoaFisica[] listaDePessoa;
	public Cadastro(int tamanho){
		listaDePessoa = new PessoaFisica[tamanho];
	}

	public void adicionarPessoa(PessoaFisica pessoa){
		for(int i=0; i<listaDePessoa.length; i++){
			if(listaDePessoa[i] == null){
				listaDePessoa[i] = pessoa;
				pessoa.setId(i);
				break;
			}
		}
	}
	// estou comentando so para teste
	public PessoaFisica getPessoaFisica(int idPosicaoDaPessoa){
		return listaDePessoa[idPosicaoDaPessoa];
	}

	public void listaDePessoas(){
		System.out.println("#### LISTANDO AS PESSOAS CADASTRADAS PARA O SORTEIO ####\n\n");
		for(PessoaFisica pessoa: listaDePessoa){
			if(pessoa !=null){
				System.out.println("Id: " + pessoa.getId() + "; Nome " + pessoa.getNome() + "; Sobrenome " + pessoa.getSobrenome() + ": Telefone " + pessoa.getTelefoneComDDD());
			}
		}
	}
}
//test: Bruno 202010020108
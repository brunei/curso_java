public class VarArgsTeste{
	public static void imprimirNumeros(int... numeros){

		//imprimir a quantidade de parametros passados ao metodo como parametro
		System.out.println("Foram passados " + numeros.length + " como parametros.");
		for(int i=0; i< numeros.length; i++){
			System.out.println("Numero: " + numeros[i]);
		}
	}	

	public static void main(String args[]){
		//invoca o metodo pasando 5 numeros inteiros
		imprimirNumeros(1,2,3,4,5);

		//invoca o metodo pasando 2 numeros inteiros
		imprimirNumeros(1,5);

		//invoca o metodo pasando 11 parametros inteiros
		imprimirNumeros(1,2,32,7,10,78,4,45,7,85,22);
	}
}
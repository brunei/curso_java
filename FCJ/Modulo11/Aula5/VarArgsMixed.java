public class VarArgsMixed{
	public static void imprimirNumeros(String mensagem, int... numeros){
		System.out.println("Invocado imprimirNumeros(String mensagem, int... numeros)");
		System.out.println("Mensagem: " + mensagem);

		System.out.println("foram passados " + numeros.length + " como parametro.");
		for(int i=0; i< numeros.length; i++){
			System.out.println("Numero: " + numeros[i]);
		}
	}

	public static void imprimirNumeros(int... numeros){
		System.out.println("Invocado imprimirNumeros(int... numeros)");
		System.out.println("Foram passados " + numeros.length + " como parametro.");
		for(int i=0; i< numeros.length; i++){
			System.out.println("Numero: " + numeros[i]);
		}
	}

	public static void imprimirNumeros(long... numeros){
		System.out.println("Invocado imprimirNumeros(long... numeros)");
		System.out.println("Foram passados " + numeros.length + " como parametro.");
		for(int i=0; i< numeros.length; i++){
			System.out.println("Numero: " + numeros[i]);
		}
	}

	public static void main(String ...args){
		imprimirNumeros(1,2,3,4);
		imprimirNumeros(1l,2l,3l,4l);
		imprimirNumeros("Teste",1,2,3,4);	
	}
}
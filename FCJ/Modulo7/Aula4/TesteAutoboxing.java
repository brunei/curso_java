public class TesteAutoboxing{
	
	public static void main(String[] args){

	Integer y = 5;
	Integer x = y;
	//Imprime true pois são o mesmo objeto
	System.out.println(y == x);

	y++;

	System.out.println("Valor de x: " + x);
	System.out.println("Valor de y: " + y);

	//Imprime false, pois são objetos diferentes
	System.out.println(y == x);
	}
}
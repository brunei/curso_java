public class TestesWrappers{
	
	public static void main(String[] args){

		//Crio 2 objetos Wrappers com o valor 127
		Integer i = 127;
		Integer x = 127;

		//Crio 2 objetos Wrappers com o valor 127 utilizando a palavra new
		Integer l1 = new Integer(127);
		Integer l2 = new Integer(127);

		//Crio 2 objetos Wrappers com o valor 128
		Integer l3 = 128;
		Integer l4 = 128;

		System.out.println("Teste i == x: " + ( i == x));
		System.out.println("Teste i.equals(x): " + i.equals(x));

		System.out.println("Teste l1 == l2: " + (l1 == l2));
		System.out.println("Teste l1.equals(l2): " + l1.equals(l2));

		System.out.println("Teste l3 == l4: " + (l3 == l4));
		System.out.println("Teste l3.equals(l4): " + l3.equals(l4));

		/*
		 *Esse teste tem o mesmo comportamento utilizando Short, Integer e long
		  */
	}
}
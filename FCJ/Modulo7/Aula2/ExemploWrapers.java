public class ExemploWrapers{
	
	public static void main (String[] args){

		//Criar um Integer passando uma String como parametro
		Integer ano = new Integer("2011");

		//Converter um Integer para uma String
		String anoString = ano.toString();

		//Converter um Integer para um Double
		double anoDouble = ano.doubleValue();

		//Converter uma String em um Long
		Long anoLong = Long.valueOf("2011");

		//Converter uma String em um long
		long anoLongPrimitivo = Long.parseLong("2011");

		System.out.println("Interger ano: " + ano);
		System.out.println("String anoString: " + anoString);
		System.out.println("double anoDouble: " + anoDouble);
		System.out.println("Long anoLong: " + anoLong);
		System.out.println("long anoLongPrimitivo: " + anoLongPrimitivo); 
	}
}
public class TesteComparacao{
	
	public static void main(String[] args){
		Integer num1 = 20;
		Integer num2 = 20;

		System.out.println("equals: " + (num1.equals(num2)));
		System.out.println("==: "+ (num1 == num2));

		Integer num3 = 200;
		Integer num4 = 200;

		System.out.println("equals : " + (num3.equals(num4)));
		System.out.println("==: " + (num3 == num4));
	}
}